import numpy as np
import shap
from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import GlobalAveragePooling2D, Conv2D, MaxPooling2D, Flatten, Dense, Dropout
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.image import resize
from tensorflow.keras.applications import VGG16, MobileNet
from tensorflow.keras.models import Model
from tensorflow.keras.utils import to_categorical
from lime import lime_image
from skimage.segmentation import mark_boundaries
import matplotlib.pyplot as plt
import gc
from loader.fashion_mnist_loader import load_preprocess_fashion_mnist_data
from loader.mnist_loader import load_preprocess_mnist_data
from loader.cifar_10_loader import load_preprocess_cifar10_data


# Define and compile the vgg-model
def create_vgg_model(base_model_fn, input_shape, num_classes):
    base_model = base_model_fn(weights='imagenet', include_top=False, input_shape=input_shape)

    # Freeze the base model
    base_model.trainable = False

    # Add custom top layers
    x = GlobalAveragePooling2D()(base_model.output)
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.5)(x)
    predictions = Dense(num_classes, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])

    return model


def create_mobilenet_model(input_shape, num_classes):
    base_model = MobileNet(input_shape=input_shape, include_top=False, weights='imagenet', pooling='avg')

    # Adjustments for fine-tuning
    for layer in base_model.layers[:-4]:
        layer.trainable = False

    # Custom top layers
    x = base_model.output
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.5)(x)
    x = Dense(512, activation='relu')(x)
    predictions = Dense(num_classes, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)
    model.compile(optimizer=Adam(learning_rate=0.0001), loss='categorical_crossentropy', metrics=['accuracy'])

    return model


# Train the model (Placeholder implementation)
def train_model(model, train_generator, val_generator, epochs=5):
    steps_per_epoch = len(train_generator)
    validation_steps = len(val_generator)
    model.fit(
        train_generator,
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        validation_data=val_generator,
        validation_steps=validation_steps
    )


def create_data_generators(train_data, train_labels, test_data, test_labels, batch_size=64):
    # Check and reshape data if necessary
    if train_data.ndim == 5:  # If there's an extra dimension
        train_data = train_data.reshape((-1, train_data.shape[2], train_data.shape[3], train_data.shape[4]))
    if test_data.ndim == 5:
        test_data = test_data.reshape((-1, test_data.shape[2], test_data.shape[3], test_data.shape[4]))

    # Create data generators
    train_datagen = ImageDataGenerator()
    test_datagen = ImageDataGenerator()

    return (
        train_datagen.flow(train_data, train_labels, batch_size=batch_size),
        test_datagen.flow(test_data, test_labels, batch_size=batch_size)
    )


def explain_predictions(model, data, labels, num_features=5, top_labels=1):
    explainer = lime_image.LimeImageExplainer()

    # Choose a random image from the data
    idx = np.random.choice(len(data))
    image = data[idx]
    label = labels[idx]

    # Ensure the image is in [0, 1] range for displaying
    image_display = image / 255.0 if np.max(image) > 1 else image

    # Generate explanation for this image
    explanation = explainer.explain_instance(image,
                                             model.predict,
                                             top_labels=top_labels,
                                             hide_color=0,
                                             num_samples=1000)

    # Get the model's prediction
    prediction = model.predict(np.array([image]))[0]
    predicted_class = np.argmax(prediction)

    # Display the original image
    plt.figure(figsize=(8, 4))
    plt.subplot(1, 2, 1)
    plt.imshow(image_display)
    plt.title(f'Original Image\nPredicted class: {predicted_class}')
    plt.axis('off')

    # Display the LIME explanation
    temp, mask = explanation.get_image_and_mask(explanation.top_labels[0],
                                                positive_only=False,
                                                num_features=num_features,
                                                hide_rest=False)
    plt.subplot(1, 2, 2)
    plt.imshow(mark_boundaries(temp, mask))
    plt.title('LIME Explanation')
    plt.axis('off')

    plt.show()


def explain_predictions_with_shap(model, data, num_samples=10):
    # Select a subset of data for explanation
    data_subset = data[:num_samples]

    # Rescale the data for visualization
    display_data_subset = (data_subset * 255).astype('uint8')

    # Initialize the SHAP explainer
    explainer = shap.GradientExplainer(model, data_subset)

    # Compute SHAP values
    shap_values = explainer.shap_values(data_subset)

    # Get model predictions
    predictions = model.predict(data_subset)
    predicted_classes = np.argmax(predictions, axis=1)

    # Print the predicted class for the first image
    predicted_class = predicted_classes[0]
    print(f'Predicted Class for the first image: {predicted_class}')

    # Plot the SHAP values for the first image in the subset
    plt.figure(figsize=(12, 6))
    shap.image_plot([shap_val[0:1] for shap_val in shap_values], display_data_subset[0:1])


def main():
    # Define which dataset to train on
    train_on_dataset = 'CIFAR-10'  # Options: 'MNIST', 'Fashion-MNIST', 'CIFAR-10'

    # Define models
    base_models = {
        'MobileNet': create_mobilenet_model
        # Add other models if needed
    }

    # Iterate over the models
    for name, model_fn in base_models.items():
        # Train on MNIST and then fine-tune on Fashion-MNIST
        if train_on_dataset == 'Fashion-MNIST':
            print(f"Training model: {name} on MNIST")
            (mnist_train_data, mnist_train_labels), (mnist_test_data, mnist_test_labels) = load_preprocess_mnist_data()
            mnist_generator, mnist_test_generator = create_data_generators(
                mnist_train_data, mnist_train_labels, mnist_test_data, mnist_test_labels, batch_size=64)

            model = model_fn((64, 64, 3), 10)
            train_model(model, mnist_generator, mnist_test_generator, epochs=10)

            print(f"Fine-tuning model: {name} on Fashion-MNIST")
            (fashion_train_data, fashion_train_labels), (
                fashion_test_data, fashion_test_labels) = load_preprocess_fashion_mnist_data()
            fashion_generator, fashion_test_generator = create_data_generators(
                fashion_train_data, fashion_train_labels, fashion_test_data, fashion_test_labels, batch_size=64)
            train_model(model, fashion_generator, fashion_test_generator, epochs=10)

            # Explain model predictions using LIME on Fashion-MNIST test data
            print(f"Explaining model predictions: {name} on Fashion-MNIST")
            X_test, y_test = next(fashion_test_generator)
            explain_predictions(model, X_test, y_test)

        # Train directly on CIFAR-10
        elif train_on_dataset == 'CIFAR-10':
            print(f"Training model: {name} on CIFAR-10")
            (cifar_train_data, cifar_train_labels), (
                cifar_test_data, cifar_test_labels) = load_preprocess_cifar10_data()
            cifar_generator, cifar_test_generator = create_data_generators(
                cifar_train_data, cifar_train_labels, cifar_test_data, cifar_test_labels, batch_size=64)

            model = model_fn((64, 64, 3), 10)
            train_model(model, cifar_generator, cifar_test_generator, epochs=10)

            # Explain model predictions using LIME on CIFAR-10 test data
            # print(f"Explaining model predictions: {name} on CIFAR-10")
            # X_test, y_test = next(cifar_test_generator)
            # explain_predictions(model, X_test, y_test)

            print(f"Explaining model predictions with SHAP: {name}")
            X_test, y_test = next(cifar_test_generator)
            explain_predictions_with_shap(model, X_test, num_samples=5)

        # Clean up
        del model
        gc.collect()


if __name__ == "__main__":
    main()
