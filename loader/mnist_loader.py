import numpy as np
from keras.datasets import fashion_mnist
from tensorflow.keras.utils import to_categorical
from tensorflow.image import resize


def load_preprocess_mnist_data(subset_size=5000):
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    # Select a subset of the data
    train_images = train_images[:subset_size]
    train_labels = train_labels[:subset_size]
    test_images = test_images[:int(subset_size * (10000 / 60000))]  # Proportionate subset size
    test_labels = test_labels[:int(subset_size * (10000 / 60000))]

    train_images_resized = resize_images(train_images, size=(64, 64))
    test_images_resized = resize_images(test_images, size=(64, 64))

    train_images_rgb = np.repeat(train_images_resized, 3, axis=-1)
    test_images_rgb = np.repeat(test_images_resized, 3, axis=-1)

    train_images_rgb = train_images_rgb.astype('float32') / 255
    test_images_rgb = test_images_rgb.astype('float32') / 255

    train_labels = to_categorical(train_labels, 10)
    test_labels = to_categorical(test_labels, 10)

    return (train_images_rgb, train_labels), (test_images_rgb, test_labels)


def resize_images(images, size=(64, 64)):
    # Resizing each image individually
    resized_images = []
    for img in images:
        # Expand dimensions from (28, 28) to (128, 128, 1)
        img_expanded = np.expand_dims(img, axis=-1)
        resized_img = resize(img_expanded, size)
        resized_images.append(resized_img)
    return np.array(resized_images)
