import numpy as np
from keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from tensorflow.image import resize


def load_preprocess_cifar10_data(subset_size=5000):
    (train_images, train_labels), (test_images, test_labels) = cifar10.load_data()

    # Select a subset of the data
    train_images = train_images[:subset_size]
    train_labels = train_labels[:subset_size]
    test_images = test_images[:int(subset_size * 0.2)]  # 20% of subset_size for test set
    test_labels = test_labels[:int(subset_size * 0.2)]

    train_images_resized = resize_images(train_images, size=(64, 64))
    test_images_resized = resize_images(test_images, size=(64, 64))

    train_images_resized = train_images_resized / 255.0
    test_images_resized = test_images_resized / 255.0

    train_labels = to_categorical(train_labels, 10)
    test_labels = to_categorical(test_labels, 10)

    return (train_images_resized, train_labels), (test_images_resized, test_labels)


def resize_images(images, size=(64, 64)):
    # Resizing each image individually
    resized_images = np.array([resize(img, size) for img in images])
    return resized_images
